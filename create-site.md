
Копирование сайта или разворот тестовой или боевой версии сайта:

1. Заходим в БД через phpmyadmin:
- для шкафов\кухонь http://pma.hoster.uplinkweb.ru 
- для цветов http://pma.hosterf.uplinkweb.ru 
- для потолков http://pma.potolki-srv.uplinkweb.ru
 

2. Создаем нового пользователя в базе данных

![картинка Screenshot_8.jpg](https://bitbucket.org/alexkor92/instructions/raw/2f1fbd261cf1f38f22b8a16f8686c8169e766b59/create-site-images/Screenshot_8.jpg)

![картинка Screenshot_1.jpg](https://bitbucket.org/alexkor92/instructions/raw/2f1fbd261cf1f38f22b8a16f8686c8169e766b59/create-site-images/Screenshot_1.jpg)

Глобальные привилегии не трогаем. Для каждого сайта свой пользователь. Имя выбирать такое же как и у домена сайта, пароль должен быть уникальным! Для тестовой и основной версий сайта использовать одного и того же пользователя, чтобы не плодить лишнего и путаницы меньше будет. Соответственно надо будет вести актуальный список пользователей.

3. Создаем базу данных. 

![картинка Screenshot_2.jpg](https://bitbucket.org/alexkor92/instructions/raw/2f1fbd261cf1f38f22b8a16f8686c8169e766b59/create-site-images/Screenshot_2.jpg)

4. В учетных записях пользоветелей редактируем привилегии у kuhni-mebel-msk. Выбираем базу данных, жмем "Вперед"

![картинка Screenshot_3.jpg](https://bitbucket.org/alexkor92/instructions/raw/2f1fbd261cf1f38f22b8a16f8686c8169e766b59/create-site-images/Screenshot_3.jpg)

Выстравляем привилегии для базы данных

![картинка Screenshot_4.jpg](https://bitbucket.org/alexkor92/instructions/raw/2f1fbd261cf1f38f22b8a16f8686c8169e766b59/create-site-images/Screenshot_4.jpg)

5. Если дамп базы данных весит меньше 2048Кб, то можно загрузить его через phpmyadmin

![картинка Screenshot_5.jpg](https://bitbucket.org/alexkor92/instructions/raw/2f1fbd261cf1f38f22b8a16f8686c8169e766b59/create-site-images/Screenshot_5.jpg)

Иначе через консоль управления сервером по SSH(putty). Также логинимся и вбиваем команду mysql -u kuhni-mebel-msk -p (Пользоваться только таким вариантом входа). Далее вводим пароль пользователя, которого создали. Потом вбиваем команды:

```
use kuhni_aran;
source path/to/you/sql/file/with/data.sql;
```

Таким образом мы залили в БД kuhni_aran все таблицы с данными (смотря что в вашем sql-файле).
Либо можно воспользоваться специальной утилитой для этого. ... Тут текст про утилиту ...


6. Создаем пользователя в операционной системе (kuhni-mebel-msk). Имя выбирать такое же как и у домена сайта, пароль должен быть уникальным!
Команда: adduser. Далее, следуя подсказкам вбиваешь данные.

```
adduser kuhni-mebel-msk
```

7. Настраиваем конфиг у сайта /var/www/uplink/kuhni-mebel-msk.ru/www/core/config/config.inc.php
В нем желательно прописать не статические пути, а динамически определять путь до основной директории с помощью dirname
Выставляем права на чтение/запись у файла /var/www/uplink/kuhni-mebel-msk.ru/www/core/config/config.inc.php: 

```
chmod 600 /var/www/uplink/kuhni-mebel-msk.ru/www/core/config/config.inc.php
```


8. Папке сайта назначаем владельца:

```
chown -R kuhni-mebel-msk:kuhni-mebel-msk /var/www/uplink/kuhni-mebel-msk.ru
```